# frozen_string_literal: true

module Vesta
  VERSION = '2.2.0'
end
